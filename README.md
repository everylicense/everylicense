# EveryLicense

Lots of [open source and free] license textfiles in as many formats of
text that I could find.


## How to Use

1. You can do whatever you want, but the way I use it is to clone this
   repository and put it on a thumbdrive or my internal network. Then I pull
   in whatever license I need when I need it.

   I usually just need plain text, but for really good doc format
   conversion, try [pandoc](http://pandoc.org)

1. It is customary to change the name of whatever license you are
   using to either LICENSE or COPYING (rather than, say,
   "GPL-v3-license.txt" or "README.gpl" or whatever).


## All Your Copyrights Are Belong to You

Note that these are not copyrights. As an author of any kind of work,
you have the inherent copyright to anything and everything you create
unless you specifically sign that right away. **None of these license
files** cause you to surrender your copyright. They are *licenses*,
meaning that they add to your copyright and tell others what they may
and may not do with your work.


### De-Mythifying the GPL and Creative Commons

Of all licenses out there, I find that people least understand those
licenses that cause others to be decent human beings. The GPL (Gnu
Public License) and Creative Commons licenses are full of variety,
spanning the full spectrum of "do whatever you want" (all-permissive
and zero, respectively) to having specific requirements to
use. Neither the GPL or CC necessarily means that work cannot be sold,
or changed, or used in derivative works; it very much depends on the
licensing terms. 

Read the license, choose to understand what it says, and use the one
that is right for your project.



### To Do...

- I want to expand this so that every open license in every reasonable
  format is here. There are lots of licenses, however, so consider that
  goal a work-in-progress that gets worked on whenever I happen to need
  a different license than those already here.

- Include summary textfiles in each dir, marking each license as
  OSI-compatible, GNU-compatible, and so on.